syntax enable                                      " Enable syntax highlighting
" colorscheme nord                                 " Nord theme
set encoding=utf-8                                 " UTF-8 encoding
set fileencoding=utf-8                             " UTF-8 encoding for saved files
set noshowmode                                     " Do not show mode (when using lightline)   
set nowrap                                         " Do not wrap line
set number                                         " Line numbers
set relativenumber				   " Relative line numbers
set ruler                                          " Mouse click won't shift cursor
set cursorline                                     " Underline the current line
" set cmdheight=2                                  " More height for commands
set t_Co=256                                       " enable 256 colors

let g:lightline = {'colorscheme': 'solarized'}
"let g:airline_theme='bubblegum'
